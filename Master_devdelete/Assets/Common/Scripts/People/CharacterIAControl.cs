﻿using FredericRP.Bucket;
using UnityEngine;
using UnityStandardAssets._2D;

namespace FredericRP.devdelete
{
    public class CharacterIAControl : MonoBehaviour
    {
        [SerializeField]
        Transform targetParent;
        [SerializeField]
        float delayBetweenTargetChange = 4;
        [SerializeField]
        float moveThreshold = 0.2f;
        [SerializeField]
        float bodyDistance = 3;
        [SerializeField]
        float minSpeed = 3;
        [SerializeField]
        float maxSpeed = 6;

        private PlatformerCharacter2D m_Character;
        private bool m_Jump;
        private Vector3 targetPosition;
        private float nextTargetChangeTime;
        PlatformerCharacter2D player;
        bool crouchNearTarget;
        bool end = false;
        BucketGenerator bucket;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
            m_Jump = false;
            nextTargetChangeTime = 0;
            crouchNearTarget = false;
            end = false;
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();

            m_Character.SetSpeed(Random.Range(minSpeed, maxSpeed));
            bucket = new BucketGenerator(30);
        }

        private void FixedUpdate()
        {
            float horizontal = 0;
            bool crouch = false;
            bool fallThrough = false;

            // Choose target
            if (player.IsDead()) {
                // Player is dead, we go see him
                if (!end)
                {
                    end = true;
                    targetPosition = player.transform.position + Vector3.right * (float)(bucket.GetRandomNumber()-15)/15 * bodyDistance;
                    crouchNearTarget = (Random.Range(0, 10) > 6);
                }
            }
            else if (targetParent != null)
            {
                // Time to change, pick a random target
                if (Time.time > nextTargetChangeTime)
                {
                    targetPosition = targetParent.GetChild(Random.Range(0, targetParent.childCount)).position;
                    nextTargetChangeTime = Time.time + delayBetweenTargetChange;
                }
            }

            // Move towards target
            if (Mathf.Abs(transform.position.x - targetPosition.x) > moveThreshold)
            {
                // Right or left
                if (targetPosition.x > transform.position.x)
                    horizontal = 1;
                else if (targetPosition.x < transform.position.x)
                    horizontal = -1;
            }
            else if (player.IsDead())
            {
                // Near the target, if player is dead, we look at him (and crouch)
                m_Character.LookAt(player.transform.position);
                if (crouchNearTarget)
                {
                    crouch = true;
                }
            }

            // Pass all parameters to the character control script.
            m_Character.Move(horizontal, crouch, m_Jump, fallThrough);
        }
    }
}