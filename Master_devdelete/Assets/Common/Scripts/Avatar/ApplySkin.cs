﻿using FredericRP.Bucket;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplySkin : MonoBehaviour {

    [SerializeField]
    List<SkinData> skinDataList;
    [SerializeField]
    List<SpriteRenderer> skinSpriteList;
    [SerializeField]
    SpriteRenderer helmetSprite;
    int skinIndex = 0;
    static BucketGenerator skinBucket;
    static int identifier = 0;

    // Use this for initialization
    void Apply(int index) {
		for(int i=0;i< skinSpriteList.Count;i++)
        {
            skinSpriteList[i].color = skinDataList[index].skinColor;
        }
        helmetSprite.sprite = skinDataList[index].helmet.sprite;
        if (helmetSprite.sprite != null)
            helmetSprite.color = skinDataList[index].helmet.color;

        SpriteRenderer[] spriteRendererList = GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < spriteRendererList.Length; i++)
        {
            spriteRendererList[i].sortingOrder += 20 * identifier;
        }
    }

    private void Start()
    {
        if (skinBucket == null)
            skinBucket = new BucketGenerator(skinDataList.Count);

        skinIndex = skinBucket.GetRandomNumber();
        identifier++;
        Debug.Log("Identifier " + identifier);
        Apply(skinIndex);
    }
}
