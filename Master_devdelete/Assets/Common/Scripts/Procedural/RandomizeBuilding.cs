﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RandomizeBuilding : MonoBehaviour {

    [SerializeField]
    int minSize = 7;
    [SerializeField]
    int maxSize = 14;
    [SerializeField]
    Color minColor = Color.black;
    [SerializeField]
    Color maxColor = Color.grey;
    [SerializeField]
    SpriteRenderer sprite;

    // Use this for initialization
    void Awake() {
        sprite.transform.localScale = new Vector3(1, Random.Range(minSize, maxSize), 1);
        sprite.color = Color.Lerp(minColor, maxColor, Random.Range(0, 1f));
    }
}
