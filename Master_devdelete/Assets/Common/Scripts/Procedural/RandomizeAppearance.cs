﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeAppearance : MonoBehaviour {

    [SerializeField]
    float threshold = 0.5f;
    [SerializeField]
    List<GameObject> gameObjectList;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < gameObjectList.Count; i++)
            gameObjectList[i].SetActive(Random.Range(0, 1f) > threshold);

    }
}
