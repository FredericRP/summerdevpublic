﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="skin", menuName ="Skin")]
public class SkinData : ScriptableObject {
    public Color skinColor;
    public Helmet helmet;

    [System.Serializable]
    public struct Helmet
    {
        public Color color;
        public Sprite sprite;
    }
}
