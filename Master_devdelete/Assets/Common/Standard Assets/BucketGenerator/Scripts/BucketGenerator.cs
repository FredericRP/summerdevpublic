﻿using UnityEngine;
using System.Collections;

namespace FredericRP.Bucket
{
    public class BucketGenerator
    {
        private int max;
        int[] randomList;
        int indexInList;

        public BucketGenerator(int max)
        {
            SetMax(max);
        }

        public void SetMax(int max)
        {
            this.max = max;
            randomList = new int[max];
            ResetBucket();
        }

        public int GetRandomNumber()
        {
            if (randomList == null)
            {
                ResetBucket();
            }
            int result = randomList[indexInList++];
            if (indexInList >= max)
            {
                indexInList = 0;
                ResetBucket();
            }
            return result;
        }

        /// <summary>
        /// Generate a list containing numbers from 0 to max, in a shuffled order
        /// </summary>
        /// <param name="max">Max.</param>
        public void ResetBucket()
        {
            indexInList = 0;

            for (int i = 0; i < max; i++)
            {
                randomList[i] = i;
            }
            // 2. Shuffle the list
            for (int i = max - 1; i > 0; i--)
            {
                int temp = randomList[i];
                int newIndex = Random.Range(0, i);
                randomList[i] = randomList[newIndex];
                randomList[newIndex] = temp;
            }
        }
    }
}
