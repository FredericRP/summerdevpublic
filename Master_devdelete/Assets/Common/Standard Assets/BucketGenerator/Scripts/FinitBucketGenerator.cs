﻿using UnityEngine;
using System.Collections.Generic;

namespace FredericRP.Bucket
{
    [System.Serializable]
    public class FinitBucketGenerator
    {
        public List<int> randomList;

        public bool IsEmpty()
        {
            if (randomList.Count == 0)
                return true;

            return false;
        }

        public int GetRandomNumber()
        {
            int result = randomList[0];
            randomList.RemoveAt(0);
            return result;
        }

        /// <summary>
        /// Generate a list containing numbers from 0 to max, in a shuffled order
        /// </summary>
        /// <param name="max">Max.</param>
        public void ResetRandomList(int max)
        {
            // 2. Shuffle the list
            for (int i = max - 1; i > 0; i--)
            {
                int temp = randomList[i];
                int newIndex = Random.Range(0, i);
                randomList[i] = randomList[newIndex];
                randomList[newIndex] = temp;
            }
        }
    }
}
