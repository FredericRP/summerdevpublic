---
type: YAML
company: FredericRP
standardAsset: Yes
name: BucketGenerator
majorversion: 0
minorversion: 1
lastModification: 2017-07-03
unityVersionMinimum: Unity 5.6.1p4

description: 
	The bucket generator provide an easy way for random round robin algorithm.

usage:
	-Instantiate a new BucketGenerator and call GetRandomNumber() to obtain a number from the bucket

implementedFeatures:
	-ResetBucket
		description: Reset the bucket with new random number

fixes:
	-NEW
		description: New Documentation, Add comments to sources.
		date: 2017-05-02
---
